library IEEE;
use IEEE.std_logic_1164.all;

--  A testbench has no ports.
entity top_tb is
end top_tb;

architecture behav of top_tb is
    component top is
    port (
        clk_i : in STD_LOGIC;
        rst_i : in STD_LOGIC;
        button_i : in STD_LOGIC_VECTOR (3 downto 0);
        led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
        led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
    );
    end component;

    for top_0: top use entity work.top;
    signal clk_i : STD_LOGIC := '0';
    signal rst_i : STD_LOGIC := '0';
    signal button_i : STD_LOGIC_VECTOR (3 downto 0) := "0000";
    signal led7_an_o : STD_LOGIC_VECTOR (3 downto 0);
    signal led7_seg_o : STD_LOGIC_VECTOR (7 downto 0);

    constant PERIOD : time := 10 ns;
begin
    top_0: top port map (
        clk_i => clk_i,
        rst_i => rst_i,
        button_i => button_i,
        led7_an_o => led7_an_o,
        led7_seg_o => led7_seg_o
    );

    clk_i <= not clk_i after PERIOD/2;

    process
    begin
        wait for 100 ns;

        for i in 1 downto 0 loop
            for j in 0 to 5 loop
                button_i(i) <= '1';
                wait for 20010 ns;  -- wait for debounce
                button_i(i) <= '0';
                wait for 4 ms;  -- wait for the display
            end loop;
        end loop;

        wait;
    end process;

end behav;
