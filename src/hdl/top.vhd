library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;


entity top is
port (
    clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    button_i : in STD_LOGIC_VECTOR (3 downto 0);
    led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
    led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
);
end top;

architecture data_flow of top is
    component kcpsm6
        generic (
            hwbuild : STD_LOGIC_VECTOR (7 downto 0) := X"00";
            interrupt_vector : STD_LOGIC_VECTOR (11 downto 0) := X"3FF";
            scratch_pad_memory_size : Integer := 64
        );
        port (
            address : out STD_LOGIC_VECTOR (11 downto 0);
            instruction : in STD_LOGIC_VECTOR (17 downto 0);
            bram_enable : out STD_LOGIC;
            in_port : in STD_LOGIC_VECTOR (7 downto 0);
            out_port : out STD_LOGIC_VECTOR (7 downto 0);
            port_id : out STD_LOGIC_VECTOR (7 downto 0);
            write_strobe : out STD_LOGIC;
            k_write_strobe : out STD_LOGIC;
            read_strobe : out STD_LOGIC;
            interrupt : in STD_LOGIC;
            interrupt_ack : out STD_LOGIC;
            sleep : in STD_LOGIC;
            reset : in STD_LOGIC;
            clk : in STD_LOGIC
        );
    end component;

    component counter
        generic (
            C_FAMILY : String := "7S";
            C_RAM_SIZE_KWORDS : Integer := 2;
            C_JTAG_LOADER_ENABLE : Integer := 0
        );
        port (
            address : in STD_LOGIC_VECTOR (11 downto 0);
            instruction : out STD_LOGIC_VECTOR (17 downto 0);
            enable : in STD_LOGIC;
            rdl : out STD_LOGIC;
            clk : in STD_LOGIC
        );
    end component;

    component display
        port (
            signal clk_i : in STD_LOGIC;
            signal rst_i : in STD_LOGIC;
            signal digit_i : in STD_LOGIC_VECTOR (31 downto 0);
            signal led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
            signal led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
        );
    end component;

    component debouncer
        port (
            signal clk_i : in STD_LOGIC;
            signal rst_i : in STD_LOGIC;
            signal raw_button_i : in STD_LOGIC;
            signal current_value : out STD_LOGIC
        );
    end component;

    function byteToLedHex(value : STD_LOGIC_VECTOR (3 downto 0)) return STD_LOGIC_VECTOR is
    begin
        case value is
            when "0000" => return "0000001";
            when "0001" => return "1001111";
            when "0010" => return "0010010";
            when "0011" => return "0000110";
            when "0100" => return "1001100";
            when "0101" => return "0100100";
            when "0110" => return "0100000";
            when "0111" => return "0001111";
            when "1000" => return "0000000";
            when "1001" => return "0000100";
            when "1010" => return "0001000";
            when "1011" => return "1100000";
            when "1100" => return "0110001";
            when "1101" => return "1000010";
            when "1110" => return "0110000";
            when "1111" => return "0111000";
            when others => return "1111111";
        end case;
    end function;

    signal address : STD_LOGIC_VECTOR (11 downto 0);
    signal instruction : STD_LOGIC_VECTOR (17 downto 0);
    signal bram_enable : STD_LOGIC;
    signal port_id : STD_LOGIC_VECTOR (7 downto 0);
    signal write_strobe : STD_LOGIC;
    signal k_write_strobe : STD_LOGIC;
    signal out_port : STD_LOGIC_VECTOR (7 downto 0);
    signal read_strobe : STD_LOGIC;
    signal in_port : STD_LOGIC_VECTOR (7 downto 0);
    signal interrupt : STD_LOGIC := '0';
    signal interrupt_ack : STD_LOGIC := '0';
    signal kcpsm6_sleep : STD_LOGIC := '0';
    signal kcpsm6_reset : STD_LOGIC := '0';

    signal digit_i : STD_LOGIC_VECTOR (31 downto 0) := (others => '1');

    signal previous_button_incr: STD_LOGIC := '0';
    signal button_incr: STD_LOGIC;
    signal previous_button_decr: STD_LOGIC := '0';
    signal button_decr: STD_LOGIC;
    signal button_pressed: Integer := 0;
begin
    pico_blaze: kcpsm6
        generic map (
            hwbuild => X"00", 
            interrupt_vector => X"3FF",
            scratch_pad_memory_size => 64
        )
        port map (
            address => address,
            instruction => instruction,
            bram_enable => bram_enable,
            port_id => port_id,
            write_strobe => write_strobe,
            k_write_strobe => k_write_strobe,
            out_port => out_port,
            read_strobe => read_strobe,
            in_port => in_port,
            interrupt => interrupt,
            interrupt_ack => interrupt_ack,
            sleep => kcpsm6_sleep,
            reset => kcpsm6_reset,
            clk => clk_i
        );

    rom: counter
        generic map (
            -- Family 'S6', 'V6' or '7S'
            C_FAMILY => "7S",
            -- Program size '1', '2' or '4'
            C_RAM_SIZE_KWORDS => 2,
            -- Include JTAG Loader when set to '1'
            C_JTAG_LOADER_ENABLE => 0
        )
        port map (
            address => address,
            instruction => instruction,
            enable => bram_enable,
            rdl => open,
            clk => clk_i
        );

    display_0: display port map (
        clk_i => clk_i,
        rst_i => '0',
        digit_i => digit_i,
        led7_an_o => led7_an_o,
        led7_seg_o => led7_seg_o
    );

    button_incr_debouncer: debouncer port map (
        clk_i => clk_i,
        rst_i => '0',
        raw_button_i => button_i(1),
        current_value => button_incr
    );

    button_decr_debouncer: debouncer port map (
        clk_i => clk_i,
        rst_i => '0',
        raw_button_i => button_i(0),
        current_value => button_decr
    );

    process (clk_i) is
    begin
        if rising_edge(clk_i) then
            if rst_i = '1' then
                kcpsm6_reset <= '1';
                in_port <= X"00";
            else
                kcpsm6_reset <= '0';

                if interrupt = '1' and interrupt_ack = '1' then
                    interrupt <= '0';
                end if;

                if button_incr = '1' and previous_button_incr = '0' then
                    -- requested action: INCREMENT
                    in_port <= X"01";
                    interrupt <= '1';
                elsif button_decr = '1' and previous_button_decr = '0' then
                    -- requested action: DECREMENT
                    in_port <= X"02";
                    interrupt <= '1';
                end if;
                -- store current values as previous values
                previous_button_incr <= button_incr;
                previous_button_decr <= button_decr;

                -- handle output from PicoBlaze
                if write_strobe = '1' and port_id = X"01" then
                    digit_i(7 downto 1) <= byteToLedHex(out_port(3 downto 0));
                end if;
            end if;
        end if;
    end process;
end data_flow;
