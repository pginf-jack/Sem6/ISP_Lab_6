library IEEE;
use IEEE.std_logic_1164.all;

entity debouncer is
port (
    clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    raw_button_i : in STD_LOGIC;
    current_value : out STD_LOGIC := '0'
);
end debouncer;

architecture data_flow of debouncer is
    signal previous_value : STD_LOGIC := '0';
    signal pending_value : STD_LOGIC := '0';

    constant STEP : Integer := 100000;  -- 1 ms
begin
    process (clk_i, rst_i) is
        variable iteration : Integer := 0;
    begin
        if rst_i = '1' then
            previous_value <= '0';
            pending_value <= '0';
            current_value <= '0';
        elsif rising_edge(clk_i) then
            pending_value <= raw_button_i;
            previous_value <= pending_value;

            if pending_value /= previous_value then
                iteration := 0;
            elsif iteration < STEP then
                iteration := iteration + 1;
            else
                current_value <= pending_value;
            end if;
        end if;
    end process;
end architecture data_flow;
