set origin_dir [file dirname [info script]]
set kcpsm6_dir "[file normalize "$origin_dir/kcpsm6"]"

set OS [lindex $tcl_platform(os) 0]

file delete -force -- "$kcpsm6_dir"
file mkdir "$kcpsm6_dir"

set old_ld_library_path ""

if { [info exists ::env(LD_LIBRARY_PATH)] } {
    set old_ld_library_path "$::env(LD_LIBRARY_PATH)"
    unset env(LD_LIBRARY_PATH)
}

set savedDir [pwd]
cd $kcpsm6_dir

if { $authUsername eq "" } {
    exec curl --no-progress-meter $kcpsm6Url --output kcpsm6.zip
} else {
    exec curl --no-progress-meter $kcpsm6Url --output kcpsm6.zip -u "$authUsername:$authPassword"
}

if { $OS == "Windows" } {
    exec tar -xf kcpsm6.zip
} else {
    exec python3 -m zipfile -e kcpsm6.zip .
}

cd $savedDir

if {"$old_ld_library_path" != ""} {
    set env(LD_LIBRARY_PATH) "$old_ld_library_path"
}
