set origin_dir [file dirname [info script]]
set src_psm_dir "[file normalize "$origin_dir/src/psm"]"
set kcpsm6_dir "[file normalize "$origin_dir/kcpsm6"]"

set OS [lindex $tcl_platform(os) 0]

set savedDir [pwd]
cd $kcpsm6_dir

file copy -force "$src_psm_dir/counter.psm" "counter.psm"

if { $OS == "Windows" } {
    exec kcpsm6.exe counter.psm
} else {
    exec wine32 kcpsm6.exe counter.psm
}

cd $savedDir
