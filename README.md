# ISP_Lab_6 - Controlling LED display using PicoBlaze processor

A microelectronic system with a Picoblaze processor, in which the hardware part performs clock frequency division and multiplexed display on an LED display.

The software reads the buttons and, depending on the button pressed, increases or decreases the counter shown on the LED display by 1.

## How to simulate?

The circuit can be simulated using Vivado:

### Simulation using Vivado

Tested with: Vivado 2018.3

The project requires Nexys A7 board files that are part of [Digilent's Vivado Board Files](https://github.com/Digilent/vivado-boards).

A Vivado project can be bootstrapped using the `build.tcl` script in the root of the repository:

1. Start Vivado.
1. Open Tcl Console (Window->Tcl Console)
1. Run commands:

   ```
   cd /path/to/repository
   set kcpsm6Url http://www.ue.eti.pg.gda.pl/isplab/Picoblaze/KCPSM6_Release9_30Sept14.zip
   set authUsername "username here"
   set authPassword "password here"
   source download_kcpsm6.tcl
   source assemble.tcl
   source build.tcl
   ```

   Please replace "username here" and "password here" with the Basic Auth username and password
   for the URL resource at `$kcpsm6Url`.

1. The project should generate shortly after which you can start the simulation
   through "Simulation->Run Simulation->Run Behavioral Simulation" option on the left.
1. If you make any changes to the file in `src/psm`, you can rebuild the relevant VHDL file from it using:

   ```
   source assemble.tcl
   ```
